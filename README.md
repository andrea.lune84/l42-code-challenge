## LARAVEL42 LIVE CODING

Create a service to query Weather API for 3 cites (CDMX, Miami, New York) with endpoint:<br>
https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&exclude={part}&appid={API key}

<ul>
    <li>Create a job to fecth the data</li>
    <li>Create a table to store the retrieved data</li>
    <li>Create a command to update the table with the forecast data and schedule the command every hour</li>
    <li>Create a simple Vue interface to show the data in a table with a select  on top to select the city</li>
</ul>

<strong>API KEY:</strong> b41c9d57aef350794d96744eb6632378

<strong>API DOC:</strong> https://openweathermap.org/current

### Coordinates
#### CDMX (19.432608, -99.133209)
#### Miami (25.761681, -80.191788)
#### New York (40.712776, -74.005974)
